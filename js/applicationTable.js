function applicationInfo() {

    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://165.232.63.158/backend/application/list-applications?all", true);
    xhttp.setRequestHeader("Content-type", "header1");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            var info = JSON.parse(this.responseText);
            var responseList = info.applications;

            makeTable(responseList);


        }
        else
        {
            document.getElementById("tableBody").innerHTML = "<tr><td> UH OH </td></tr>";
        }
    };


    xhttp.send();

}


function makeTable(responseList) {
    var retStr = "";
    if (responseList.length == 0) {
        retStr = "No applications are currently open."
    }

    for(i = 0; i<responseList.length; i++) {
        var app = responseList[i];


        retStr += "<tr><td>"+app.user_email+"</td><td>"+app.station_name+"</td><td>"+app.proof+"</td><td>"+app.app_submitted+"</td><td><button onclick='submitDecision("+app.app_id+", true)' class='w3-button w3-green'>Accept</button><button onclick='submitDecision("+app.app_id+", false)' class='w3-button w3-red'>Decline</button></td></tr>";
    }
    document.getElementById("tableBody").innerHTML = retStr;

}

function submitDecision(app_id, decision) {
    var appInfo = {"command": 'decide', "app_id": app_id, "decision": decision};


    strAppInfo = JSON.stringify(appInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://165.232.63.158/backend/admin/application", true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() {

        if(this.readyState == 4 && this.status == 200) {
            alert("Successfully Submitted");
            window.location.href="/dashboard/";
        }
    };


    xhttp.send(strAppInfo);
}


applicationInfo();