// Handle drag and drop into a dropzone_element div:
// send the files as a POST request to the server
"use strict";
Dropzone.autoDiscover = false;

var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, {
    
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    uploadMultiple: true,
    autoProcessQueue: false,
    url: "../backend/upload",
    previewsContainer: "#previews",
    clickable: ".fileinput-button"
});
myDropzone.ondrop = function(e) {
            // Stop browser from simply opening that was just dropped
            e.preventDefault();  
};

document.getElementById("btnSubmit").addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            var toUpload = myDropzone.files;
            var data = {"flightDate": document.getElementById("flightDate").value, "station": 'USTX00001'};
            upload_files(toUpload, data);
        });

document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true);
};

// Update the total progress bar
      myDropzone.on("totaluploadprogress", function(progress) {
        document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
      });

      myDropzone.on("sending", function(file) {
        // Show the total progress bar when upload starts
        document.querySelector("#total-progress").style.opacity = "1";
        // And disable the start button
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
      });
      myDropzone.on("addedfile", function(file) {
        // Hookup the start button
        file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
      });

      // Hide the total progress bar when nothing's uploading anymore
      myDropzone.on("queuecomplete", function(progress) {
        document.querySelector("#total-progress").style.opacity = "0";
      });

      // Setup the buttons for all transfers
      // The "add files" button doesn't need to be setup because the config
      // `clickable` has already been specified.
      document.querySelector("#actions .start").onclick = function() {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
      };
      document.querySelector("#actions .cancel").onclick = function() {
        myDropzone.removeAllFiles(true);
      };


function upload_files(files, data) {


    var minSteps = 6,
        maxSteps = 60,
        timeBetweenSteps = 100,
        bytesPerStep = 100000;

        var self = this;

        for (var i = 0; i < files.length; i++) {

          var file = files[i];
          var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

          for (var step = 0; step < totalSteps; step++) {
            var duration = timeBetweenSteps * (step + 1);
            setTimeout(function(file, totalSteps, step) {
              return function() {
                file.upload = {
                  progress: 100 * (step + 1) / totalSteps,
                  total: file.size,
                  bytesSent: (step + 1) * file.size / totalSteps
                };

                myDropzone.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
                if (file.upload.progress == 100) {
                  file.status = Dropzone.SUCCESS;
                  myDropzone.emit("success", file, 'success', null);
                  myDropzone.emit("complete", file);
                }
              };
            }(file, totalSteps, step), duration);
          }
        }

    let formData = new FormData(),
        xhr = new XMLHttpRequest();
    formData.append("flightDate", data["flightDate"]);
    formData.append("station", data["station"]);

    for(let i=0; i<files.length; i++) {
        formData.append("file", files[i]);
    }

    xhr.onreadystatechange = function() {
        if(xhr.readyState === XMLHttpRequest.DONE) {
            alert(xhr.responseText);
        }
    }



    xhr.open('POST', '../backend/upload', true); // async = true
    xhr.send(formData); 
}
