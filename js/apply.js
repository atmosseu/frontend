function submitApplication() {
    var sess_id = getCookie("sess_id");

    var appInfo = {"command": 'submit', "sess_id": sess_id, "name": document.getElementById('name').value, "state": document.getElementById('state').value, "country": document.getElementById('country').value, "latitude": document.getElementById('latitude').value, "longitude": document.getElementById('longitude').value, "elevation": document.getElementById('elevation').value};


    strAppInfo = JSON.stringify(appInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://165.232.63.158/backend/application", true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            alert("Successfully Submitted");
            window.location.href="/dashboard/";
        }
    };


    xhttp.send(strAppInfo);
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}