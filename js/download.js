//will receive data from dashboard selection and the it will grab
//all the data from the database and once it somes back it will send
//the output to the download.html to display 
//make connection to database first 

function populateTable(items){
    const table = document.getElementById("cell");
    items.forEach( item => {
      let row = table.insertRow();
      let date = row.insertCell(0);
      date.innerHTML = item.date;
      let name = row.insertCell(1);
      name.innerHTML = item.name;
    });
}