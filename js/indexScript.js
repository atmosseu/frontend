var btnLogout = document.getElementById("btnLogout");
if(btnLogout != null) {
    btnLogout.addEventListener("click", function() {
            submitLogout();
        });
}

var btnLogin = document.getElementById("btnLogin");
if(btnLogin != null) {
   document.addEventListener("keyup", function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            btnLogin.click();
        }
   });
}
// Program Description: A user fills out the login form, the login info is then put in a dictionary,
//                      the dictionary is turned into a JSON readable string, the string is then passed t0
//                      runHTTPRequest.py
// Pre: The user has filled out the login form and pressed the submit button
// Post: A response is recieved from runHTTPRequest.py
function submitLogin()
{
    var flag = window.location.host
    var username = document.getElementById('user').value.trim();
    var password = document.getElementById('pass').value.trim();
    if (username == "" || password == "") {
        return;
    }
    var loginInfo = {'username': username, 'password': password};


    strLoginInfo = JSON.stringify(loginInfo);


    var xhttp = new XMLHttpRequest();


    xhttp.open("POST", "http://165.232.63.158/backend/login", true);
    xhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    xhttp.withCredentials = true;
    xhttp.send(strLoginInfo);

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            response = JSON.parse(this.response);
            sess_id = response['sess_id'];
            auth_type = response['user_type'];
            document.cookie = "sess_id=" + sess_id;
            document.cookie = "user_type=" + auth_type;
            window.location.href="/dashboard/";


        }
        else if(this.readyState == 4 && this.status == 418)
        {
            window.alert("Incorrect username or password. Please try again.");
        }
    };

}


// Program Function: Takes information submitted in the new user form, inserts info into a dictionary,
//                   turns dictionary into a JSON readable string, creates a new XMLHttpRequest, sends the
//                   JSON readable string to runHTTPRequest.py
// Pre: New user form is filled out and the user presses submit button
// Post: A response message is recieved fom runHTTPRequest.py
function submitLogout()
{   

    var sess_id = getCookie("sess_id");
    var userInfo = {'sess_id': sess_id};


    strUserInfo = JSON.stringify(userInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://165.232.63.158/backend/logout", true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            document.cookie = "sess_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            document.cookie = "user_type=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            alert("Successfully Logged Out");
            window.location.href="/";
        }
    };


    xhttp.send(strUserInfo);
}

// Program Function: Takes information submitted in the new user form, inserts info into a dictionary,
//                   turns dictionary into a JSON readable string, creates a new XMLHttpRequest, sends the
//                   JSON readable string to runHTTPRequest.py
// Pre: New user form is filled out and the user presses submit button
// Post: A response message is recieved fom runHTTPRequest.py
function submitRegistration()
{   

    var flag = window.location.host
    var userInfo = {"flag": flag.charAt(0), "username": document.getElementById('user').value, "password": document.getElementById('pass').value, "firstName": document.getElementById('firstName').value, "lastName": document.getElementById('lastName').value, "email": document.getElementById('email').value};


    strUserInfo = JSON.stringify(userInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://165.232.63.158/backend/register", true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            alert("Successfully Registered");
            window.location.href="/";
        }
    };


    xhttp.send(strUserInfo);
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
